#pragma once
#include <sstream>
#include <iostream>
#include <vector>
#include <map>
#include "Interval.h"
#include "Transition.h"
#include "Lexeme.h"

class Automat
{
public:
    // Raw: for scaner, Lexeme: for syntax
    enum InputFormat { Raw, Lexemes };
    /// <summary> Automat constructor. </summary>
    /// <param name="alphabet"> The alphabet of the automat. </param>
    /// <param name="states"> The states of the automat. NOTE: states[0] will be initial state! Must be non-empty. </param>
    /// <param name="transitions"> Transition list for the automat. Must correspond with the given alphabet and states </param>
    /// <param name="outputRules"> Rules for output function of the automat. Input of transitions
    /// must be from stetes and alphabet, outputState must be from Automat::outputActions</param>
    Automat(const Alphabet &alphabet,
        const std::vector<std::string> &states,
        const std::vector<Transition> &transitions, const InputFormat format, const std::vector<Transition> &outputRules = std::vector<Transition>());
    
    std::vector<Lexeme> processInput(const std::string &input);

    static const char elseCharacter;
    static const std::vector<std::string> outputActions;

private:
    void setCurrentState(const std::string &newState);

    void addToLexeme(char character);
    void clearLexeme();
    void writeLexemeToOutput(const std::string &lexemeType);
    void backspace();

    void processInputCharacter(const char input);
    void processInputLongCharacter(const std::string &longInput);
    void outputFunction(const std::string &state, const char inputCharacter);
    void outputFunction(const std::string & state, const std::string inputLongCharacter);

    Lexeme findInNameTable(int lexemeId);
    bool createOrFindInNameTable(const std::string &lexemeName, const std::string &lexemeType, Lexeme &outFoundLexeme);
    int getMaxIdNameTable();

    Alphabet alphabet;
    std::vector<std::string> states;
    std::vector<Transition> transitions;
    // digits, letters, etc
    std::map<std::string, std::vector<char>> characterCategories;

    std::string initialState;
    std::string currentState;
    std::string currentLexemeName;
    Lexeme currentLexeme;
    std::vector<Transition> outputRules;
    
    InputFormat inputFormat;
    std::stringstream inputStream;
    std::vector<Lexeme> output;
    std::vector<Lexeme> nameTable;
};
