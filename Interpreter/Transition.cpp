#include "Transition.h"


Transition::Transition(const std::vector<std::string> &inputStates, const std::vector<char> &inputCharacters, const std::string &outputState)
{
    this->inputStates = inputStates;
    this->inputCharacters = inputCharacters;
    this->outputState = outputState;
}


Transition::Transition(const std::vector<std::string>& inputStates, const char & inputCharacter, const std::string & outputState)
    : Transition(inputStates, std::vector<char>{ inputCharacter }, outputState)
{
}

Transition::Transition(const std::string &inputState, const std::vector<char>& inputCharacters, const std::string & outputState)
    : Transition(std::vector<std::string>{ inputState }, inputCharacters, outputState)
{
}

Transition::Transition(const std::string &inputState, const char &inputCharacter, const std::string &outputState)
    : Transition(std::vector<std::string>{ inputState }, std::vector<char>{ inputCharacter }, outputState)
{
}

bool Transition::checkIsCorrect(const Alphabet &alphabet, const std::vector<std::string> &states, const char elseCharacter) const
{
    // check if input/output state belongs to states, input character belongs to alphabet
    bool belongsToAlphabet = alphabet.containsRange(this->inputCharacters) ||
        (this->inputCharacters.size() == 1 && this->inputCharacters[0] == elseCharacter);
    bool outputBelongsToStates = std::find(states.begin(), states.end(), this->outputState) != states.end();
    
    bool inputBelongsToStates = true;
    for (auto state : this->inputStates)
    {
        if (std::find(states.begin(), states.end(), state) == states.end())
        {
            inputBelongsToStates = false;
            break;
        }
    }

    if (!belongsToAlphabet || !inputBelongsToStates || !outputBelongsToStates)
    {
        return false;
    }
    return true;
}

bool Transition::checkIsCorrect(const Alphabet & alphabet, const std::vector<std::string>& states, const std::vector<std::string>& outputStates, const char elseCharacter) const
{
    auto allStates = std::vector<std::string>(states);
    allStates.insert(allStates.end(), outputStates.begin(), outputStates.end());
    return checkIsCorrect(alphabet, allStates, elseCharacter);
}

bool Transition::compareInput(const std::string &inputState, const char &inputCharacter) const
{
    if (std::find(this->inputStates.begin(), this->inputStates.end(), inputState) == this->inputStates.end() ||
        std::find(this->inputCharacters.begin(), this->inputCharacters.end(), inputCharacter) == this->inputCharacters.end())
    {
        return false;
    }
    return true;
}

bool Transition::compareInput(const std::string & inputState, const std::string & inputLongCharacter) const
{
    if (std::find(this->inputStates.begin(), this->inputStates.end(), inputState) == this->inputStates.end() ||
        std::find(this->inputLongCharacters.begin(), this->inputLongCharacters.end(), inputLongCharacter) == this->inputLongCharacters.end())
    {
        return false;
    }
    return true;
}

std::string Transition::getOutputState() const
{
    return outputState;
}
