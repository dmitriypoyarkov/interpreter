#include "Interval.h"

std::vector<char> Interval::getCharactersRange() const
{
    auto characters = std::vector<char>();
    for (int i = this->lowerBound; i <= this->upperBound; i++)
    {
        characters.push_back(i);
    }
    return characters;
}
