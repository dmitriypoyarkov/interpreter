#pragma once
#include <iostream>
#include <vector>
#include <map>
#include "Interval.h"
#include "Alphabet.h"
class Transition
{
public:
    Transition(const std::string &inputState, const char &inputCharacter, const std::string &outputState);
    Transition(const std::vector<std::string> &inputStates, const char &inputCharacter, const std::string &outputState);
    Transition(const std::string &inputState, const std::vector<char> &inputCharacters, const std::string &outputState);
    Transition(const std::string &inputState, const std::string &inputString, const std::string &outputState);
    Transition(const std::vector<std::string> &inputStates, const std::string &inputString, const std::string &outputState);
    Transition(const std::string &inputState, const std::vector<std::string> &inputStrings, const std::string &outputState);
    bool checkIsCorrect(const Alphabet &alphabet, const std::vector<std::string> &states, const char elseCharacter) const;
    bool checkIsCorrect(const Alphabet &alphabet, const std::vector<std::string> &states, const std::vector<std::string> &outputStates, const char elseCharacter) const;
    
    // true if input in parameters is equal to this->input
    bool compareInput(const std::string &inputState, const char &inputCharacter) const;
    bool compareInput(const std::string &inputState, const std::string &inputLongCharacter) const;

    std::string getOutputState() const;

private:
    // base constructor; can be called only from public constructors.
    // the reason is to disallow multiple inputStates and multiple inputCharacters at the same time.
    Transition(const std::vector<std::string> &inputStates, const std::vector<char> &inputCharacters, const std::string &outputState);
    
    std::vector<std::string> inputStates;
    std::vector<char> inputCharacters;
    // all inputs convert to strings
    std::vector<std::string> inputLongCharacters;

    std::string outputState;
};

