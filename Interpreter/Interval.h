#pragma once
#include <vector>
class Interval
{
public:
    Interval()
    {
        this->lowerBound = 0;
        this->upperBound = 0;
    }
    Interval(int lowerBound, int upperBound)
    {
        this->lowerBound = lowerBound;
        this->upperBound = upperBound;
    }

    int getLowerBound() const
    {
        return lowerBound;
    }

    int getUpperBound() const
    {
        return upperBound;
    }

    bool contains(char item) const
    {
        if (item <= this->getLowerBound() || this->getUpperBound() <= item)
        {
            return false;
        }
        return true;
    }

    bool containsRange(std::vector<char> range) const
    {
        for (auto item : range)
        {
            if (!(this->contains(item)))
            {
                return false;
            }
        }
        return true;
    }

    bool containsRange(std::vector<int> &range) const
    {
        for (auto item : range)
        {
            if (!(this->contains(item)))
            {
                return false;
            }
        }
        return true;
    }

    std::vector<char> getCharactersRange() const;
private:
    int lowerBound;
    int upperBound;
};

