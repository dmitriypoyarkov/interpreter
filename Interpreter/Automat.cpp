#include "Automat.h"
#include <algorithm>

Automat::Automat(const Alphabet &alphabet,
    const std::vector<std::string> &states,
    const std::vector<Transition> &transitions,
    const InputFormat format, const std::vector<Transition> &outputRules)
{
    for (auto transition : transitions)
    {
        if (!transition.checkIsCorrect(alphabet, states, elseCharacter))
        {
            throw std::exception("Automat Creation: Invalid transition list for this alphabet and/or states.");
        }
    }
    for (auto rule : outputRules)
    {
        if (!rule.checkIsCorrect(alphabet, states, outputActions, elseCharacter))
        {
            throw std::exception("Automat Creation: Invalid outputRules list for this alphabet and/or states.");
        }
    }
    if (states.size() == 0)
    {
        throw std::exception("Automat Creation: There must be at least one state");
    }
    this->initialState = states[0];
    this->alphabet = alphabet;
    this->states = states;
    this->transitions = transitions;
    this->outputRules = outputRules;
    this->inputFormat = format;
    this->output = std::vector<Lexeme>();
    this->inputStream = std::stringstream();
    this->currentLexemeName = std::string();
}

std::vector<Lexeme> Automat::processInput(const std::string & input)
{
    output.clear();
    setCurrentState(initialState);

    inputStream << input;
    do
    {
        char nextChar = inputStream.get();
        /*if (inputStream.eof())
        {
            break;
        }*/
        // next state
        processInputCharacter(nextChar);
    } while (!inputStream.eof());
    
    return this->output;
}

void Automat::setCurrentState(const std::string &newState)
{
    for (auto state : this->states)
    {
        if (newState == state)
        {
            this->currentState = newState;
            return;
        }
    }
    throw std::exception("Automat setCurrentState error: attempt to change to invalid state");
}

void Automat::addToLexeme(char character)
{
    currentLexemeName.push_back(character);
}

void Automat::clearLexeme()
{
    currentLexemeName.clear();
}

void Automat::writeLexemeToOutput(const std::string &lexemeType)
{
    output.push_back(Lexeme(currentLexemeName, lexemeType));
}

void Automat::backspace()
{
    if (inputStream.eof())
    {
        return;
    }
    inputStream.unget();
    if (inputStream.fail())
    {
        throw std::exception("Automat backspace error: stream was on the beginning");
    }
}

void Automat::processInputCharacter(const char input)
{
    std::string currentInput = std::to_string(input);
    if (inputFormat == InputFormat::Lexemes)
    {
        // input is id, get lexemeName
        this->currentLexeme = findInNameTable(input);
        currentInput = findInNameTable(input).getName;
        
    }
    processInputLongCharacter(std::to_string(input));
}

void Automat::processInputLongCharacter(const std::string &longInput)
{    
    for (auto transition : transitions)
    {
        if (transition.compareInput(this->currentState, longInput))
        {
            setCurrentState(transition.getOutputState());
            outputFunction(this->currentState, longInput);
            return;
        }
    }
    //if there are no transitions for character, check elseCharacter
    for (auto transition : transitions)
    {
        if (transition.compareInput(this->currentState, elseCharacter))
        {
            setCurrentState(transition.getOutputState());
            outputFunction(this->currentState, input);
            return;
        }
    }

    throw std::exception("Automat nextInput error: there are no transitions for given input");
}

void Automat::outputFunction(const std::string & state, const char inputCharacter)
{
    this->outputFunction(state, std::to_string(inputCharacter));
}

void Automat::outputFunction(const std::string & state, const std::string inputLongCharacter)
{
    auto action = std::string();
    for (auto rule : outputRules)
    {
        if (rule.compareInput(state, inputLongCharacter))
        {
            action = rule.getOutputState();
        }
    }
    for (auto rule : outputRules)
    {
        if (rule.compareInput(state, elseCharacter))
        {
            action = rule.getOutputState();
        }
    }
    if (action == "add")
    {
        addToLexeme(inputLongCharacter);
    }
    if (action == "clr")
    {
        clearLexeme();
    }
    if (action == "write")
    {
        // lookupLexemeInNameTable
        auto findInNameTable(inputLongCharacter);
        // adjustLexemeId
        writeLexemeToOutput(state);
        clearLexeme();
        backspace();
        setCurrentState(initialState);
    }
}

Lexeme Automat::findInNameTable(int lexemeId)
{
    auto foundLexeme = std::find_if(nameTable.begin(), nameTable.end(),
        [&lexemeId](const Lexeme &lexeme) { return lexeme.getId() == lexemeId; });
    if (foundLexeme == nameTable.end())
    {
        std::exception("findInNameTable: no lexeme with such id");
    }
    return *foundLexeme;
}

bool Automat::createOrFindInNameTable(const std::string &lexemeName,
                                      const std::string &lexemeType,
                                      Lexeme &outFoundLexeme)
{
    auto foundLexeme = std::find_if(nameTable.begin(), nameTable.end(),
        [&lexemeName](const Lexeme &lexeme) { return lexeme.getName() == lexemeName; });
    if (foundLexeme == nameTable.end())
    {
        outFoundLexeme = Lexeme(lexemeName, lexemeType, getMaxIdNameTable() + 1);
        nameTable.push_back(outFoundLexeme);
        return false;
    }
    outFoundLexeme = *foundLexeme;
    return true;
}

int Automat::getMaxIdNameTable()
{
    auto it = std::max_element(nameTable.begin(), nameTable.end());
    if (it == nameTable.end())
    {
        std::exception("getMaxIdNameTable: nameTable is empty!");
    }
    return it->getId();
}

const char Automat::elseCharacter = -1;
const std::vector<std::string> Automat::outputActions = { "clr", "add", "write", "return" };
