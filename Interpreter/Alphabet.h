#pragma once
#include <vector>
class Alphabet
{
public:
    Alphabet();
    Alphabet(const std::vector<char> &characters, const char elseCharacter = -1);
    Alphabet(const std::vector < std::string > &longCharacters, const char elseCharacter = -1);
    bool contains(char character) const;
    bool containsRange(const std::vector<char> &range) const;
    bool contains(const std::string &longCharacter) const;
    bool containsRange(const std::vector<std::string> &longCharactersRange) const;
private:
    std::vector<char> characters;
    std::vector<std::string> longCharacters;
    char elseCharacter;
};
