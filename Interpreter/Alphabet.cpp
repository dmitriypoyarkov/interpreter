#include "Alphabet.h"
#include <string>

Alphabet::Alphabet()
{
    this->characters = std::vector<char>();
    this->elseCharacter = -1;
}

Alphabet::Alphabet(const std::vector<char> &characters, const char elseCharacter)
{
    this->characters = characters;
    this->elseCharacter = elseCharacter;
}

bool Alphabet::contains(char character) const
{
    std::string longCharacter = std::to_string(character);
    return this->contains(longCharacter);
}

bool Alphabet::containsRange(const std::vector<char> &range) const
{
    for (auto item : range)
    {
        if (!(this->contains(std::to_string(item))))
        {
            return false;
        }
    }
    return true;
}

bool Alphabet::contains(const std::string &longCharacter) const
{
    return std::find(longCharacters.begin(), longCharacters.end(), longCharacter) != longCharacters.end();
}

bool Alphabet::containsRange(const std::vector<std::string> &longCharactersRange) const
{
    for (auto item : longCharactersRange)
    {
        if (!(this->contains(item)))
        {
            return false;
        }
    }
    return true;
}
