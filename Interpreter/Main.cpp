﻿#include <string>
#include <iostream>
#include "Automat.h"
#include "Lexeme.h"

int main()
{
    auto keywordTable = std::vector<std::string>{ "int" };
    auto nameTable = std::vector<Lexeme>();
    
    //Alphabet
    auto digits = Interval(48, 57).getCharactersRange();
    auto letters = Interval(65, 90).getCharactersRange();
    auto lowerCaseLetters = Interval(97, 122).getCharactersRange();
    letters.insert(letters.end(), lowerCaseLetters.begin(), lowerCaseLetters.end());
    auto spaces = std::vector<char>{ 9, ' ' }; //space, tab
    auto equals = std::vector<char>{ '=' };
    auto characters = Interval(0, 127).getCharactersRange();
    auto operators = std::vector<char>{ '+', '-', '*', '/', '=' };
    auto alphabet = Alphabet(characters);

    //States
    auto states = std::vector<std::string>{ "start", "letter-digit", "keyword-identifier", "operator-begin",
                                            "operator", "number", "float-number", "integer", "float" };
    
    //Transitions
    auto transitions = std::vector<Transition>();
    //keyword-identifier
    transitions.push_back(Transition("start", letters, "letter-digit"));
    transitions.push_back(Transition("letter-digit", letters, "letter-digit"));
    transitions.push_back(Transition("letter-digit", digits, "letter-digit"));
    transitions.push_back(Transition("letter-digit", Automat::elseCharacter, "keyword-identifier"));
    //integer
    transitions.push_back(Transition("start", digits, "number"));
    transitions.push_back(Transition("number", digits, "number"));
    transitions.push_back(Transition("number", Automat::elseCharacter, "integer"));
    //float
    transitions.push_back(Transition("number", '.', "float-number"));
    transitions.push_back(Transition("float-number", digits, "float-number"));
    transitions.push_back(Transition("float-number", Automat::elseCharacter, "float"));
    //operator
    transitions.push_back(Transition("start", operators, "operator-begin"));
    transitions.push_back(Transition("operator-begin", Automat::elseCharacter, "operator"));

    for (auto state : states)
        if (state != "letter-digit")
            transitions.push_back(Transition(state, spaces[1], state));

    //Output rules
    auto outputRules = std::vector<Transition>();
    outputRules.push_back(Transition("letter-digit", letters, "add"));
    outputRules.push_back(Transition("letter-digit", digits, "add"));
    outputRules.push_back(Transition("keyword-identifier", Automat::elseCharacter, "write"));
    outputRules.push_back(Transition("number", digits, "add"));
    outputRules.push_back(Transition("float-number", '.', "add"));
    outputRules.push_back(Transition("integer", Automat::elseCharacter, "write"));
    outputRules.push_back(Transition("float-number", digits, "add"));
    outputRules.push_back(Transition("float", Automat::elseCharacter, "write"));
    outputRules.push_back(Transition("operator-begin", operators, "add"));
    outputRules.push_back(Transition("operator", Automat::elseCharacter, "write"));
    
    try
    {
        const std::string input = "int a = 1.1";
        std::cout << "Input sequence is:" << std::endl;
        std::cout << input <<std::endl;

        //Automat creation
        Automat scanner = Automat(alphabet, states, transitions, Automat::InputFormat::Raw, outputRules);

        auto output = scanner.processInput(input);

        std::cout << "Output sequence is:" << std::endl;
        for (auto item : output)
        {
            std::cout << item.toString() << std::endl;
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
}