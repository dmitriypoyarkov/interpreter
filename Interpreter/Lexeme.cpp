#include "Lexeme.h"

Lexeme::Lexeme(const std::string &name, const std::string &type, const char id)
{
    this->name = name;
    this->type = type;
    this->id = id;
}

void Lexeme::setId(char id)
{
    this->id = id;
}

int Lexeme::getId() const
{
    return this->id;
}

std::string Lexeme::getName() const
{
    return name;
}

std::string Lexeme::toString() const
{
    return "Lexeme { id: " + std::to_string(this->id) +
           ", name: " + this->name +
           ", type: " + this->type + " }";
}
