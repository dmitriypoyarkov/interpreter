#pragma once
#include <iostream>
#include <string>
class Lexeme
{
public:
    Lexeme(const std::string &name, const std::string &type, const char id = 0);

    void setId(char id);
    int getId() const;
    std::string getName() const;
    std::string toString() const;
private:
    char id;
    std::string name;
    std::string type;
};

